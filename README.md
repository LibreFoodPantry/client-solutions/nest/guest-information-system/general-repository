LibreFood Pantry
LibreFood Pantry — A food pantry for the Worcester State University community.
This Documentation project contains details about the LibreFood Pantry client solution, including design, development processes, and licensing.
Note: LibreFood Pantry uses main for the default branch, rather than master.


User Stories — Stories about how the intended users of the software currently work, used to guide the design of the software.

Architecture — A high-level design for the pieces of the software system and how they interact with each other.

Technology — A listing of the tools and frameworks we have decided to use to build and deploy the software system.

WorkflowWorkflow — The workflow to be used by developers.

Release Process — Release process for microservices.
Licenses — We license all our code under GPLv3 and all other content under CC-BY-SA 4.0.

Copyright © 2021 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
